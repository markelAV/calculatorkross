import React, {Component} from 'react'
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {Container, Header, Left, Body, Right, Content, Footer, FooterTab, Button, Icon, Title} from 'native-base';
import {Col, Row, Grid} from "react-native-easy-grid";
import {deleteStack} from "react-native/Libraries/LogBox/Data/LogBoxSymbolication";
import {IMLocalized, init} from '../locale/IMLocalized';

var id = 1;
export default class Calculator extends Component {

    constructor(props) {
        super(props);
        this.state = {
            stringState: "",
            stringResult: "0",
            result: 0,
            operationComplete: true,
            operation: "+",
            operand: 0,
            id:1
        };
    }

    _buttonNumeralClick(button) {
        console.log(button);
        let flagClear = this.state.stringState ==="" && parseFloat(this.state.stringResult) === 0
            && this.state.stringResult.indexOf(".") < 0;
        if (!this.state.operationComplete) {
            if (this.state.stringState.indexOf("=") >= 0) {
                this.setState({stringState: ""});
                this.setState({operation: "+"});
                this.setState({operand: 0});
                this.setState({stringResult: "0"});
                this.setState({result: 0});
            } else {
                if (!parseInt(this.state.stringState[this.state.stringState.length - 1])) {
                    this.setState({operation: this.state.stringState[this.state.stringState.length - 1]});
                }
                console.log("current operation is " + this.state.stringState[this.state.stringState.length - 1]);
            }
            this.setState({operationComplete: true});
            flagClear = true;
        }

        console.log(this.state.stringResult);
        if (button !== "." && flagClear) {
            this.setState({stringResult: button});
        } else {
            if (button === "." && this.state.stringResult.indexOf(".") >= 0) {
                console.log("Warning dont add dot");
            } else {
                this.setState({stringResult: this.state.stringResult + button});
            }
        }
    }

    _buttonOperationClick(button) {
        console.log(button);
        switch (button) {
            case "AC":
                this.setState({
                    stringState: "",
                    stringResult: "0",
                    result: 0,
                    operationComplete: true,
                    operation: '+',
                    operand: 0,
                    id:1
                });
                break;
            case "del":
                if(this.state.operationComplete) { // Todo Check that operation is selected
                    if (this.state.stringResult.length > 1) {
                        console.log("length" + this.state.stringResult.length);
                        let text = this.state.stringResult.slice(0, -1);
                        this.setState({stringResult: text});
                    }
                    else {
                        this.setState({stringResult: "0"});
                    }
                }
                break;
            case "+/-":
                if(this.state.operationComplete) {
                    if(this.state.stringResult[0] === "-"){
                        this.setState({stringResult: this.state.stringResult.substring(1)});
                    }
                    else {
                        this.setState({stringResult: "-" + this.state.stringResult});
                    }
                }
                break;
            default :
                if (this.state.operationComplete) {
                    let operand = parseFloat(this.state.stringResult);
                    this.setState({operand: operand});
                    console.log("result1 is " + this.state.stringResult);
                    console.log("result2 is " + parseFloat(this.state.stringResult));
                    console.log("result33 is " + this.state.operand);
                    this.setState({operationComplete: false});
                    this.setState({stringState: this.state.stringState + this.state.stringResult + button});
                    console.log("state string is " + this.state.stringState);
                    this.count(operand);
                } else {
                    if (this.state.stringState.indexOf("=") >= 0) {
                        if (button === "=") {
                            console.log(this.state.operand);
                            this.changeFirstOperand();
                            this.count(this.state.operand);
                        }
                        else {
                            this.setState({stringState: this.state.stringResult + button});
                        }
                    }
                    else {
                        //Fixme please !!!!
                        let text = this.state.stringState.slice(0, -1);
                        text = text + button;
                        console.log(text);
                        this.setState({stringState: text});
                    }
                }

        }
    }

    count() {
        let result = 0.0;
        switch (this.state.operation) {
            case "+":
                result = this.state.result + this.state.operand;
                break;
            case "-":
                result = this.state.result - this.state.operand;
                break;
            case "*":
                result = this.state.result * this.state.operand;
                break;
            case "/":
                if(this.state.operand !== 0) {
                    result = this.state.result / this.state.operand;
                }
                else {
                    console.log("Деление на ноль невозможно "); //Todo complete case with resolve
                }
                break;
            case "^":
                result = this.state.result ** this.state.operand;
                break;
        }

        console.log(this.state.result + " " + this.state.operation + " " + this.state.operand + " = " + result);
        this.setState({result: result});
        this.setState({stringResult: result.toString()});
    }
    count(operand) {
        let result = 0.0;
        switch (this.state.operation) {
            case "+":
                result = this.state.result + operand;
                break;
            case "-":
                result = this.state.result - operand;
                break;
            case "*":
                result = this.state.result * operand;
                break;
            case "/":
                if(operand !== 0) {
                    result = this.state.result / operand;
                }
                else {
                    console.log("Деление на ноль невозможно "); //Todo complete case with resolve
                }
                break;
            case "^":
                result = this.state.result ** operand;
                break;
        }

        console.log(this.state.result + " " + this.state.operation + " " + operand + " = " + result);
        this.setState({result: result});
        this.setState({stringResult: result.toString()});
    }

    changeFirstOperand() {
        //Fixme please!! bad practice
        let str = this.state.stringState;
        console.log("str " + str);
        let i = str.length -1;
        if(str[i] === '=') {
            i--; //Fixme
        }
        while(i > 0 && (str[i] === "." || !isNaN(parseInt(str[i])))) {
            i--;
        }
        console.log("index i is " + i);
        if(i > 0) {
            str = this.state.stringResult + str.substring(i, str.length);
            console.log("str2 " + str);
            this.setState({stringState: str});
        }
    }

    _getSaveString() {
        let result = null;
        if (this.state.stringState.indexOf("=") >= 0) {
            result = this.state.stringState + this.state.result;
        }
        return result
    }

    _generateBody() {
        this.setState({id: this.state.id++});
        id ++;
        let str = null;
        let resutl = this._getSaveString();
        if (resutl != null) {
            str = "{ \"id\":\"" + id + "\", \"result\": \"" + resutl + "\"}";
        }

        return str;
    }

    _onClickSaveButton() {
        var body = this._generateBody();
        if (body != null) {
            console.log(body);
            var request = new XMLHttpRequest();
            request.onreadystatechange = (e) => {
                if (request.readyState !== 4) {
                    return;
                }

                if (request.status === 200) {
                    console.log('success', request.responseText);
                } else {
                    console.warn('error');
                }
            };

            request.open('POST', 'http://35.239.172.200:80/history/operation/add');
            request.setRequestHeader("Content-type", "application/json; utf-8");
            request.setRequestHeader("Accept", "application/json");
            request.send(body);
            alert("Operation save success");
        }
    }
    render() {
        init();
        return (
            <Container>
                <Header>
                    <Left/>
                    <Body>
                        <Title>{IMLocalized('calculator')}</Title>
                    </Body>
                    <Right>
                        <Button style={{}} transparent  onPress={()=>this._onClickSaveButton()}>
                            <Text style={{color:"#408AD2"}}>{IMLocalized('save')}</Text>
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <Grid>
                        <Row style={{flex:1, flexDirection:'row-reverse', alignItems: 'flex-end'}}>
                            <Text style={{fontSize: 18}}>
                                {this.state.stringState}
                            </Text>
                        </Row>
                        <Row style={{flex:1, flexDirection:'row-reverse', alignItems: 'flex-end'}}>
                            <Text style={{fontSize: 50}}>{this.state.stringResult}</Text>
                        </Row>
                        <Row style={{flex: 1, alignItems: 'center', justifyContent: 'center', marginLeft: 10}}>
                            <Col>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonOperationClick("AC");
                                    }}>
                                        <View style={styles.buttonOperation2}>
                                            <Text style={styles.textOperation2}>AC</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonNumeralClick("7");
                                    }}>
                                        <View style={styles.buttonNumeral}>
                                            <Text style={styles.textNumeral}>7</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonNumeralClick("4");
                                    }}>
                                        <View style={styles.buttonNumeral}>
                                            <Text style={styles.textNumeral}>4</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonNumeralClick("1");
                                    }}>
                                        <View style={styles.buttonNumeral}>
                                            <Text style={styles.textNumeral}>1</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonNumeralClick("0");
                                    }}>
                                        <View style={styles.buttonNumeral}>
                                            <Text style={styles.textNumeral}>0</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                            </Col>
                            <Col>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonOperationClick("+/-");
                                    }}>
                                        <View style={styles.buttonOperation2}>
                                            <Text style={styles.textOperation2}>±</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonNumeralClick("8");
                                    }}>
                                        <View style={styles.buttonNumeral}>
                                            <Text style={styles.textNumeral}>8</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonNumeralClick("5");
                                    }}>
                                        <View style={styles.buttonNumeral}>
                                            <Text style={styles.textNumeral}>5</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonNumeralClick("2");
                                    }}>
                                        <View style={styles.buttonNumeral}>
                                            <Text style={styles.textNumeral}>2</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonNumeralClick(".");
                                    }}>
                                        <View style={styles.buttonNumeral}>
                                            <Text style={styles.textNumeral}>.</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                            </Col>
                            <Col>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonOperationClick("^");;
                                    }}>
                                        <View style={styles.buttonOperation2}>
                                            <Text style={styles.textOperation2}>x^y</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonNumeralClick("9");
                                    }}>
                                        <View style={styles.buttonNumeral}>
                                            <Text style={styles.textNumeral}>9</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonNumeralClick("6");
                                    }}>
                                        <View style={styles.buttonNumeral}>
                                            <Text style={styles.textNumeral}>6</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonNumeralClick("3");
                                    }}>
                                        <View style={styles.buttonNumeral}>
                                            <Text style={styles.textNumeral}>3</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonOperationClick("del");
                                    }}>
                                        <View style={styles.buttonNumeral}>
                                            <Text style={styles.textNumeral}>⌫</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                            </Col>
                            <Col>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonOperationClick("/");
                                    }}>
                                        <View style={styles.buttonOperation2}>
                                            <Text style={styles.textOperation2}>/</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonOperationClick("*");
                                    }}>
                                        <View style={styles.buttonOperation2}>
                                            <Text style={styles.textOperation2}>X</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonOperationClick("-");;
                                    }}>
                                        <View style={styles.buttonOperation2}>
                                            <Text style={styles.textOperation2}>-</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonOperationClick("+");;
                                    }}>
                                        <View style={styles.buttonOperation2}>
                                            <Text style={styles.textOperation2}>+</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                                <Row>
                                    <TouchableOpacity onPress={() => {
                                        this._buttonOperationClick("=");
                                    }}>
                                        <View style={styles.buttonOperation1}>
                                            <Text style={styles.textOperation1}>=</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Row>
                            </Col>
                        </Row>
                    </Grid>
                </Content>
                <Footer>
                    <FooterTab>
                        <Button onPress={() => this.props.navigation.navigate('History')}>
                            <Icon name="ios-list"/>
                        </Button>
                        <Button active>
                            <Icon active name="calculator"/>
                        </Button>
                        <Button onPress={() => this.props.navigation.navigate('Settings')}>
                            <Icon name="ios-settings"/>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonNumeral: {
        width: 64,
        height: 64,
        backgroundColor: '#b1b5b5',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 2
    },
    buttonOperation1: {
        width: 64,
        height: 64,
        backgroundColor: '#c44e1a',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 2
    },
    buttonOperation2: {
        width: 64,
        height: 64,
        backgroundColor: '#abeae4',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 2
    },
    textNumeral: {
        fontSize: 20
    },
    textOperation1: {
        fontSize: 20,
        color: '#ffffff'
    },
    textOperation2: {
        fontSize: 20,
        color: '#000000'
    }

});
