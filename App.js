import { StatusBar } from 'expo-status-bar';
import { Root } from "native-base";
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Calculator from './screens/Calculator'
import History from './screens/History'
import Settings from './screens/Settings'

const Stack = createStackNavigator();

export default function App() {
  return (
      <Root>
          <NavigationContainer>
              <Stack.Navigator initialRouteName="Calculator" screenOptions={{headerShown: false}}>
                  <Stack.Screen name="Calculator" component={Calculator} />
                  <Stack.Screen name="History" component={History} />
                  <Stack.Screen name="Settings" component={Settings} />
              </Stack.Navigator>
          </NavigationContainer>
      </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
