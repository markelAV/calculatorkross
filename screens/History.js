import React, { Component } from 'react';
import {Image, View} from 'react-native';
import {Body, Button, Container, Content, Footer, FooterTab, Header, Icon, Left, Right, Text, Title, List, ListItem, FlatList} from 'native-base';
import {IMLocalized, init} from '../locale/IMLocalized';
import {updateId} from "expo-updates";

var flagUpdate =false;
var ids = [];
var listResults = []
var example = ["1+1=3","1+1=2","1+1=4","1+1=5","1+1=6"]
export default class History extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    init_load() {
        flagUpdate = true;
        var request = new XMLHttpRequest();
        request.onreadystatechange = (e) => {
            if (request.readyState !== 4) {
                return;
            }

            if (request.status === 200) {

                console.log('success', request.responseText);
                let tasks = JSON.parse(request.responseText);
                console.log(tasks);
                let flagNotUpdate = listResults.length === tasks.length;
                let responseIds = []
                let responseResult = []
                for(let j =0; j < tasks.length; j++){
                    responseIds.push(tasks[j].id);
                    responseResult.push(tasks[j].result);
                }
                ids = responseIds;
                listResults = responseResult;
                console.log(listResults);
                console.log(ids);
                if(!flagNotUpdate) {
                    this.forceUpdate();
                }
            } else {
                console.warn('error');
            }
        };

        request.open('GET', 'http://35.239.172.200:80/history/operations');
        request.send();

    }

    _click_clear() {
        if(ids != null && ids.length > 0) {
            for(let i = 0; i< ids.length; i++) {
                let idRes = ids[i];
                var request = new XMLHttpRequest();
                request.onreadystatechange = (e) => {
                    if (request.readyState !== 4) {
                        return;
                    }

                    if (request.status === 200) {
                        console.log('success', request.responseText);
                        this.forceUpdate();
                    } else {
                        console.warn('error');
                    }
                };

                request.open('GET', 'http://35.239.172.200:80/history/operation/delete?id_operation=' + idRes);
                request.send();
            }
        }
        alert("Clear successful");
    }
    _renderRow = (item) => {
        return (
            <ListItem style={{ flex: 1 }}>
                <Text style={{ marginLeft: 25 }}>{item}</Text>
            </ListItem>
        )
    }

    render() {
        init();
        this.init_load();
        return (
            <Container>
                <Header>
                    <Left/>
                    <Body>
                        <Title>{IMLocalized('history')}</Title>
                    </Body>
                    <Right>
                        <Button style={{}} transparent  onPress={()=>this._click_clear()}>
                            <Text style={{color:"#408AD2"}}>{IMLocalized('clear')}</Text>
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <List
                        dataArray={listResults} renderRow={(item) => this._renderRow(item)}
                    />
                </Content>
                <Footer>
                    <FooterTab>
                        <Button active>
                            <Icon name="ios-list" />
                        </Button>
                        <Button onPress={() => this.props.navigation.navigate('Calculator')}>
                            <Icon active name="calculator" />
                        </Button>
                        <Button onPress={() => this.props.navigation.navigate('Settings')}>
                            <Icon name="ios-settings" />
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}
