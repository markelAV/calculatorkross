import React, { Component } from 'react';
import {Image, View} from 'react-native';
import {IMLocalized, init} from '../locale/IMLocalized';
import {
    Body,
    Button,
    Container,
    Content, Footer,
    FooterTab,
    Header,
    Icon,
    Left,
    ListItem,
    Right,
    Switch,
    Text,
    Title,
    ActionSheet
} from 'native-base';
import * as Localization from "expo-localization";

var BUTTONSLocale = ["English", "Русский", "Український", "Беларускі", "Cancel"];
var DESTRUCTIVE_INDEXLocale = 3;
var CANCEL_INDEXLocale = 4;

var BUTTONSLimit = ["5", "4", "3", "2", "Cancel"];
var DESTRUCTIVE_INDEXLimit = 3;
var CANCEL_INDEXLimit = 4;

export default class Settings extends Component {

    changeLocale(language) {
        let locale = "";
        switch (language) {
            case "English":
                locale ="en-US";
                break;
            case "Русский":
                locale ="ru-RU";
                break;
            case "Український":
                //Todo complete
                break;
            case "Беларускі":
                //Todo complete
                break;
        }
        console.log("select1 " + language + " " + locale)

    }
    render() {
        init();
        return (
            <Container>
                <Header>
                    <Left/>
                    <Body>
                        <Title>{IMLocalized('settings')}</Title>
                    </Body>
                    <Right>
                    </Right>
                </Header>
                <Content>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: "#007AFF" }}  onPress={() =>
                                ActionSheet.show(
                                    {
                                        options: BUTTONSLimit,
                                        cancelButtonIndex: CANCEL_INDEXLimit,
                                        destructiveButtonIndex: DESTRUCTIVE_INDEXLimit,
                                        title: IMLocalized('roundingLimit')
                                    },
                                    buttonIndex => {
                                        this.setState({ clicked: BUTTONSLimit[buttonIndex] });
                                    }
                                )}>
                                <Icon active name="wifi" />
                            </Button>
                        </Left>
                        <Body>
                            <Text>{IMLocalized('roundingLimit')}</Text>
                        </Body>
                        <Right>
                            <Text>5</Text>
                        </Right>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: "#007AFF" }} onPress={() =>
                                ActionSheet.show(
                                    {
                                        options: BUTTONSLocale,
                                        cancelButtonIndex: CANCEL_INDEXLocale,
                                        destructiveButtonIndex: DESTRUCTIVE_INDEXLocale,
                                        title: IMLocalized('chooseLanguage')
                                    },
                                    buttonIndex => {
                                        this.changeLocale(BUTTONSLocale[buttonIndex]);
                                    }
                                )}>
                                <Icon active name="text" />
                            </Button>
                        </Left>
                        <Body>
                            <Text>{IMLocalized('language')}</Text>
                        </Body>
                        <Right>
                            <Text>{Localization.locale.substring(3)}</Text>
                        </Right>
                    </ListItem>
                </Content>
                <Footer>
                    <FooterTab>
                        <Button onPress={() => this.props.navigation.navigate('History')}>
                            <Icon name="ios-list" />
                        </Button>
                        <Button onPress={() => this.props.navigation.navigate('Calculator')}>
                            <Icon active name="calculator" />
                        </Button>
                        <Button active>
                            <Icon name="ios-settings" />
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}
